'use strict';

document.addEventListener('DOMContentLoaded', function () {

    var percentage = document.querySelector('.js_percentage');

    lory(percentage, {
        infinite: 1
    });

});

var eat = ['yum!', 'gulp', 'burp!', 'nom'];
var yum = document.createElement('p');
var msie = /*@cc_on!@*/0;
yum.style.opacity = 1;

var links = document.querySelectorAll('li > span'), el = null;
for (var i = 0; i < links.length; i++) {
    el = links[i];

    el.setAttribute('draggable', 'true');

    addEvent(el, 'dragstart', function (e) {
        e.dataTransfer.effectAllowed = 'copy'; // only dropEffect='copy' will be dropable
        e.dataTransfer.setData('Text', this.id); // required otherwise doesn't work
    });
}

var bin = document.querySelector('#bin');
var bin2 = document.querySelector('#bin2');

addEvent(bin, 'dragover', function (e) {
    if (e.preventDefault) e.preventDefault(); // allows us to drop
    this.className = 'over';
    e.dataTransfer.dropEffect = 'copy';
    return false;
});

// to get IE to work
addEvent(bin, 'dragenter', function (e) {
    this.className = 'over';
    return false;
});

addEvent(bin, 'dragleave', function () {
    this.className = '';

});

addEvent(bin, 'drop', function (e) {
    if (e.stopPropagation) {
        e.stopPropagation();
    } // stops the browser from redirecting...why???
    e.preventDefault();

    var el = document.getElementById(e.dataTransfer.getData('Text'));

    el.parentNode.removeChild(el);

    // stupid nom text + fade effect
    bin.className = 'happy';
    bin2.className = 'happy';
    yum.innerHTML = eat[parseInt(Math.random() * eat.length)];

    var y = yum.cloneNode(true);
    bin.appendChild(y);

    setTimeout(function () {
        var t = setInterval(function () {
            if (y.style.opacity <= 0) {
                if (msie) { // don't bother with the animation
                    y.style.display = 'none';
                }
                clearInterval(t);
            } else {
                y.style.opacity -= 0.1;
            }
        }, 50);
    }, 250);

    return false;
});

var sheet = document.createElement('style'),
    $rangeInput = $('.range input'),
    prefs = ['webkit-slider-runnable-track', 'moz-range-track', 'ms-track'];

document.body.appendChild(sheet);

var getTrackStyle = function (el) {
    var curVal = el.value,
        val = (curVal - 1) * 16.666666667,
        style = '';

    // Set active label
    $('.range-labels li').removeClass('active selected');

    var curLabel = $('.range-labels').find('li:nth-child(' + curVal + ')');

    curLabel.addClass('active selected');
    curLabel.prevAll().addClass('selected');

    return style;
};

$rangeInput.on('input', function () {
    sheet.textContent = getTrackStyle(this);
});

// Change input value on label click
$('.range-labels li').on('click', function () {
    var index = $(this).index();

    $rangeInput.val(index + 1).trigger('input');

});

$('.bottom__form-buttons a').click(function () {
    $(this).addClass('active');
    $(this).siblings().removeClass('active');
    var wrap2 = $('.wrapper2');
    if ($(this).hasClass('day')) {
        // console.log('day');
        wrap2.find('ф.day').addClass('active');
        wrap2.find('a.day').siblings().removeClass('active');
    }
    if ($(this).hasClass('week')) {
        wrap2.find('a.week').addClass('active');
        wrap2.find('a.week').siblings().removeClass('active');
    }
    if ($(this).hasClass('month')) {
        wrap2.find('a.month').addClass('active');
        wrap2.find('a.month').siblings().removeClass('active');
    }
});
